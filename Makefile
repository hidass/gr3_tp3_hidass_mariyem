IDIR = include
ODIR = obj
SDIR = src

CC = gcc
CFLAGS = -g -Wall -std=c99 -I$(IDIR)

PROG = listen

_DEPS = list.h # Toutes les dépendances
# On ajoute include/ à chaque dépendance
DEPS = $(patsubst %,$(IDIR)/%,$(_DEPS))

_OBJ = list.o main.o # Toutes les dépendances
# On ajoute obj/ à chaque dépendance
OBJ = $(patsubst %,$(ODIR)/%,$(_OBJ))

# Toutes les étiquettes qui ne sont pas des fichiers
# sont déclarées ici
.PHONY: run all clean mrproper

run : $(PROG)
	./$(PROG)

all : $(PROG) # others app can be build here

# La cible est l'exécutable listen
# qui dépent des objets $(OBJ)
$(PROG): $(OBJ)
	# $@ désigne la cible $(PROG) == listen
	# $^ désigne les dépendances $(OBJ) == list.o main.o
	$(CC) $(CFLAGS) -o $@ $^

# La cible est un objet (se trouvant dans le répertoire obj)
# Les dépendances sont $(DEPS) et le fichier source : $(SDIR)/%.c
# où % désigne le nom de la cible sans le suffixe .o
# qui se situe dans le répertoire $(ODIR)
$(ODIR)/%.o: $(SDIR)/%.c $(DEPS)
	$(CC) $(CFLAGS) -c -o $@ $<

# les cibles de nettoyage
clean:
	rm -f $(ODIR)/*.o $(SDIR)/*.back core $(IDIR)/*.back

mrproper: clean
	rm $(PROG)
