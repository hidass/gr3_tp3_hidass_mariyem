#include "elmlist.h"
#include "list.h"
#include "outils.h"

struct list {
struct elmlist * head;
struct elmlist * tail;
int numelm;
};
/**
DÉCLARATIONS DES FONCTIONS PUBLIQUES
**/
/ Vérifie si la liste est vide ou pas */
bool estVide(const struct list* L) {
    return L == NULL ;
}

struct list* consVide() {
    return NULL;
}

void cons(struct list * L, void * d) {

    if ( estVide(L) == true){
        L=(struct list*) malloc(sizeof(struct list));
        L->head = (struct elmlist*) malloc(sizeof(struct elmlist));
        L->head->data=d;
        L->tail= L->head;//une liste d'un seul element => debut=fin donc on pointe tail sur head
    }
    else{
        struct elmlist * p = L->head;
        L->head =  (struct elmlist*) malloc(sizeof(struct elmlist));
        L->head->data=d;//on insert la donnée dedans
        L->head->suc = p ; // on lie le nouveau maillon avec le reste de la liste
    }

    L->numelm++; //on incrémente le nombre d'élément de L par 1

}
void queue(struct list * L, void * d){
    struct elmlist * p  = (struct elmlist*) malloc(sizeof(struct elmlist)); //on crée un nouveau element 
    p->data = d;  //on insert la donnée dedans
    L->tail->suc = p; // on lie le nouveau tail avec l'avant dernier tail
    L->tail=p ;// on fait pointer la liste sur le nouveau tail ainsi il devient la nouvelle queue
    L->numelm++; //on incrémente le nombre d'élément de L par 1
};

struct elmlist * getHead(struct list * L){
    return L->head; //renvoi la tete de la liste
};

struct elmlist * getTail(struct list * L){
    return L->tail; //renvoi la queue de la liste
};

int getNumelm(struct list * L){
    return L->numelm; //renvoi le nombre d'element de la liste
};






void insert_ordered(struct list* L, void* d, bool (ptrf) ()) {

    if (estVide(L) || d < L->head->data) {
        cons(L, d);
    }
    else {
        struct elmlist p = L->head;

        while (p->suc != NULL && (*ptrf)(p->suc->data, d)) {
            p = p->suc;
        }
        insert_after(L, d, p);
    }
}




void insert_after(TListe * L, void * d, TElement * place){
    if(estVide(L) || !place){
        cons(L,d);
    }else
    {
        TELement * E = (TELement *) calloc (1, sizeof(TELement));
        E->data = d;
        E->suc = palce -> suc;
        place -> suc =E;
    }
    
}

void viewlist(struct elmlist * L ) {
  printf( "[ " );
  for( struct elmlist * iterator = L; iterator; iterator = iterator->suc ) {
    printf( "%d ", iterator->x );
  }
  printf( "]\n\n" );
}

void rmList(struct list L, void (*ptrf)(), bool withData){
    if (withData){
        TELement * E;
        for (int i = 1 ; i <L->numelm ; i++){
            E=L->head->data);
            //free(L->head);
            L->head=E;
        }
    free(L);
}