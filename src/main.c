#include <stdio.h>
#include <stdlib.h>

#include "list.h"
#include "elmlist.h"
#include "outils.h"

#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

void listehomoreelle() {
    void (*ptrPrint) (double*);
    void (*ptrRm) (double*);
    bool (*ptrCmp) (double* , double*);
    struct list* L = consVide();
    double* v;
    double u;

    ptrPrint = &printDouble; // pointeur sur l�afficha d�un r�el
    ptrRm = &rmDouble; // pointeur sur la lib�ration d�un r�el
    ptrCmp = &reelcmp;
    do {
        
        printf("Entrez un reel (0 pour s'arreter): ");
        scanf("%lf", &u);
        if (u == 0) break;
        v = (double*)calloc(1, sizeof(double));
        *v = u;
        //cons(L, v);

        insert_ordered(L, v, ptrCmp);

    } while (true);
    viewList(L, ptrPrint);
    rmList(L, ptrRm, true);
}

int main()
{
    listehomoentiere();
    listehomoreelle();

    getchar();
    return 0;
}
