/**************
TYPES ABSTRAITS
**************/
/** Le type d’un élément de liste :
- ‘x‘ un *pointeur* sur une donnée,
- ‘suc‘ un pointeur sur son successeur (ou NULL s’il n’y en a pas)
*/

#ifndef __ELMLIST_H__
#define __ELMLIST_H__
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

struct elmlist {
void * data;
struct elmlist * suc;
};
/***********************************
DÉCLARATIONS DES FONCTIONS PUBLIQUES
***********************************/
/** Récupérer la *donnée* d’un élément de liste */
void * getData ( struct elmlist * E );
/** Récupérer le *successeur* d’un élément de liste */
struct elmlist * getSuc ( struct elmlist * E );
/** Modifier la *donnée* d’un élément de liste */
void setData (struct elmlist * E, void * d );
/** Modifier le *successeur* d’un élément de liste */
void setSuc ( struct elmlist * E, struct elmlist * S );

#endif 

