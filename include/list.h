/**************
TYPES ABSTRAITS
**************/
/** Le type d’un élément de liste :
- ‘head‘ un *pointeur* sur le premier élément de la liste
- ‘tail‘ un *pointeur* sur le dernier élément de la liste
- ‘numelm‘ le nombre d’éléments rangés dans la liste
*/

#ifndef __LIST_H__
#define __LIST_H__
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

struct list {
struct elmlist * head;
struct elmlist * tail;
int numelm;
};
/***********************************
DÉCLARATIONS DES FONCTIONS PUBLIQUES
***********************************/
/** Vérifie si la liste est vide ou pas */
bool estVide(struct list * L );
/** Construit une liste vide */
struct list * consVide();
/** Ajoute en tête de la liste un nouvel élément dont la donnée est ‘d‘ */
void cons(struct list L, void * d){

    struct elmlist *A = (struct elmlist *) calloc(1,sizeof(struct elmlist));

    A->data =d;
    A->suc = L->head;

    L->head = A;

    if(!L->tail){

        L->tail=A;
    }

    L->numelm++;

}

/** Ajoute en queue de la liste un nouvel élément dont la donnée est ‘d‘ */
void queue(struct list * L, void * d);
/** Consulte la tête de la liste */
struct elmlist * getHead(struct list * L);
/** Consulte la queue de la liste */
struct elmlist * getTail(struct list * L);
/** Consulte le nombre d’éléments rangés dans la liste */
int getNumelm(struct list * L);

#endif 